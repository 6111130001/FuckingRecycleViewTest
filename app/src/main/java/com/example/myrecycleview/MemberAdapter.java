package com.example.myrecycleview;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.Viewholder> {

    ArrayList<String> arrayListOfMember;
   // ArrayList<String> theword;

    public MemberAdapter(ArrayList arrayListOfMember) {

        this.arrayListOfMember = arrayListOfMember;

       // Log.d("Say","ser"+this.arrayListOfMember);

    }

    @NonNull
    @Override

    public MemberAdapter.Viewholder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.the_card,parent,false);

        return new MemberAdapter.Viewholder(view);

    }

    @NonNull
    @NotNull


    @Override
    public void onBindViewHolder(@NonNull @NotNull MemberAdapter.Viewholder holder, int position) {

    holder.textView_name.setText(arrayListOfMember.get(position));
//    holder.imageView.;



    }




    @Override
    public int getItemCount() {
        return arrayListOfMember.size();
    }

    private int genPictureNum() {
        int i = (0) ;

        switch (i) {
            case 1:
                return R.drawable.gun_wing_man1;
            case 2:
                return R.drawable.gun_wingman_2;
            case 3:
                return R.drawable.gun_wingman_3;
            case 4:
                return R.drawable.gun_wingman_4;
            case 5:
                return R.drawable.gun_wingman_5;
            case 6:return R.drawable.mr_gun1;
            case 7:return R.drawable.mr_gun2;
            case 8:return R.drawable.mrs_gun1;
            case 9:return R.drawable.gun_mao_eye;
            default:return R.drawable.gun_glasses;
        }
    }




    public class Viewholder extends RecyclerView.ViewHolder {

        TextView textView_name;
        ImageView imageView;

        public Viewholder(@NonNull @NotNull View itemView) {
            super(itemView);
            textView_name = itemView.findViewById(R.id.textView_name);
            imageView = itemView.findViewById(R.id.imageView);

        }


    }

}

