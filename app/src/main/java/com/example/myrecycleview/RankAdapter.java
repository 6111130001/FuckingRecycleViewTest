package com.example.myrecycleview;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class RankAdapter extends RecyclerView.Adapter<RankAdapter.Viewholder> {
    Activity activity;
    ArrayList<String> arrayListOfRankList;
    Boolean isTheTemperIsNulls = true;

    ArrayList<String> temper;
  //  private Viewholder holder;

    //ArrayList<String> arrayListName ;
//    public RankAdapter(ArrayList<String> temper,int i) {
//
//    }

    public RankAdapter(ArrayList<String> arrayListOfRankList, ArrayList<String> temper) {
        this.temper  =temper;

        this.arrayListOfRankList = arrayListOfRankList;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rank_recycle,parent,false);

        return new RankAdapter.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RankAdapter.Viewholder holder, int position) {
//        ArrayList<String> theword = new ArrayList<>();
//        theword = this.temper;
        ArrayList<String> arrayListOfMember = new ArrayList<>();
        ArrayList<String> justAdminName = new ArrayList<>();// normal ชื่อ
        justAdminName.add("gun");
        justAdminName.add("boom");
        justAdminName.add("pon");
        justAdminName.add("por");
        justAdminName.add("not");
        ArrayList<String> justMemberName = new ArrayList<>();
        justMemberName.add("sarun");
        justMemberName.add("nuntiput");
        justMemberName.add("songpon");
        justMemberName.add("thanadon");
        justMemberName.add("supakrit");


//       Log.d("RankADT Say:", "temper.size() = "+ temper.size());
//////////////////////////////////////////////////////////////////
        if (temper.size() == 0) {
            isTheTemperIsNulls =false;
            Log.d("RankADT Say:", "temper is null");
            temperSetName();
        }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (position == 0) {//int b =0;
                Log.d("RankSay", "on if 1 ");
                holder.textView_Rank.setText(arrayListOfRankList.get(position));

                for (String copy : justAdminName) {//*** :
                   Log.d("temper",  "  จะเชคคำว่า = " + copy + " ในตัวแปล (copy1)");
                 //  ตัวที่ถูกนำไปเปรียบเทียบ   .contain     ตัวที่เป็นแบบให้เทียบ

                    Log.d("temper",  " isTheTemperIsNulls =  " +isTheTemperIsNulls);

                    if (isTheTemperIsNulls == true){
                        Log.d("temper",  " เข้า " +isTheTemperIsNulls+ " มาแล้ว");

                        if (copy.           contains    ( this.temper.get(temper.size()-1))) {
                            Log.d("temper", "จะออกมาว่า" + copy);
                            Log.d("temper", "Hi Mr.Pickle");
                            arrayListOfMember.add(copy);
                        }

                    }else if( isTheTemperIsNulls == false){
                        Log.d("temper",  " เข้า " +isTheTemperIsNulls+ " มาแล้ว // temper,copy = "+ temper+ " "+ copy);

                        if (this.temper.contains(copy)) {
                        Log.d("RankSay", "Hi Mr Pickle2" + this.temper);
                        arrayListOfMember.add(copy);

                         }
                    }

                }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                //////////////////////////////////////////////////////
                //            for (int i = 1; i <= 5; i++) {
                //
                //                arrayListOfMember.add("Admin :"+i);
                //            }
                //            arrayListOfMember.add("gun");
                //            arrayListOfMember.add("boom");
                //            arrayListOfMember.add("pon");
                //            arrayListOfMember.add("por");
                //            arrayListOfMember.add("not");

                MemberAdapter goToAdapterMember = new MemberAdapter(arrayListOfMember);
                LinearLayoutManager layoutManagerMember = new LinearLayoutManager(activity);
                holder.recycler_member.setLayoutManager(layoutManagerMember);
                holder.recycler_member.setAdapter(goToAdapterMember);

            }
            else if (position == 1) {
                holder.textView_Rank.setText(arrayListOfRankList.get(position));
                for (String copy : justMemberName) {

                    if (isTheTemperIsNulls == true){
                        if (copy.           contains    ( this.temper.get(temper.size()-1))) {
                            Log.d("temper", "จะออกมาว่า" + copy);
                            Log.d("temper", "Hi Mr.Pickle");
                            arrayListOfMember.add(copy);
                        }

                    }else if( isTheTemperIsNulls == false){
                        if (this.temper.contains(copy)) {
                            Log.d("RankSay", "Hi Mr Pickle2" + this.temper);
                            arrayListOfMember.add(copy);

                        }
                    }

                }
                MemberAdapter goToAdapterMember = new MemberAdapter(arrayListOfMember);
                LinearLayoutManager layoutManagerMember = new LinearLayoutManager(activity);
                holder.recycler_member.setLayoutManager(layoutManagerMember);
                holder.recycler_member.setAdapter(goToAdapterMember);

            }//else if (position == 1)

    }

    public void temperSetName() {
        this.temper.add("gun");
        this.temper.add("boom");
        this.temper.add("pon");
        this.temper.add("por");
        this.temper.add("not");
        //
        this.temper.add("sarun");
        this.temper.add("nuntiput");
        this.temper.add("songpon");
        this.temper.add("thanadon");
        this.temper.add("supakrit");

    }



    @Override
    public int getItemCount() {
        return arrayListOfRankList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView textView_Rank;
        RecyclerView recycler_member;

        public Viewholder(@NonNull @NotNull View itemView) {
            super(itemView);

            textView_Rank = itemView.findViewById(R.id.textView_Rank);
            recycler_member=itemView.findViewById(R.id.recycler_member);
        }
    }
}
